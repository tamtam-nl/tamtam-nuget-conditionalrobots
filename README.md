# README #

This package shows a robots.txt that disallows all searchengines if the url ends with tamtam.nl. The standard robots.txt can already contain the correct values for the live url, allowing search engines. The package is build on the Rewrite Extension for IIS7+ (http://www.iis.net/downloads/microsoft/url-rewrite).

Simply replace Tam Tam with your company test domain and you're good to go.

DtSearch Spider is allowed for all urls

### PACKAGE ###

The package installs the following:

* It adds a robots.txt and a robots_closed.txt to the root of the website
* It adds a rewrite rule to the web.config that rewrites the robots.txt to robots_closed.txt for all urls ending with tamtam.nl